const express = require('express');

const app = express();

app.use(express.json())

app.use('/', (req, res, next) => {
    res.send({ message: 'beyondcars test2 test3' })
})

app.use('/info', (req, res, next) => {
    res.send({ message: 'beyondcars info' })
})

app.listen(3000, () => {
    console.log("server start at port 3000")
});
