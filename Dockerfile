# base image
FROM node:alpine

# create app directory
WORKDIR /app

COPY . /app

# install app dependencies
RUN npm install

EXPOSE 3000

CMD ["node", "index.js"]
